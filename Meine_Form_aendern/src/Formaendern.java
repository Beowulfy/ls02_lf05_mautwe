import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JTextArea;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Font;

public class Formaendern extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Formaendern frame = new Formaendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Formaendern() {
		setBackground(Color.LIGHT_GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 375, 693);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Beispieltext....:(");
		lblNewLabel.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblNewLabel.setBounds(12, 13, 333, 52);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblNewLabel_1.setBounds(29, 78, 316, 16);
		contentPane.add(lblNewLabel_1);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getContentPane().setBackground(Color.RED);
			}
		});
		btnRot.setBounds(22, 107, 97, 25);
		contentPane.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getContentPane().setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(131, 107, 97, 25);
		contentPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getContentPane().setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(246, 107, 97, 25);
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getContentPane().setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(22, 145, 97, 25);
		contentPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getContentPane().setBackground(Color.LIGHT_GRAY);
			}
		});
		btnStandardfarbe.setBounds(131, 145, 97, 25);
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe wh\u00E4len");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getContentPane().setBackground(JColorChooser.showDialog(null, 
			            "Farbauswahl", null));
			}
		});
		btnFarbeWhlen.setBounds(246, 145, 97, 25);
		contentPane.add(btnFarbeWhlen);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 13));
			}
		});
		btnArial.setBounds(22, 198, 97, 25);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
			}
		});
		btnComicSansMs.setBounds(136, 198, 97, 25);
		contentPane.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Courier New", Font.PLAIN, 13));
			}
		});
		btnCourierNew.setBounds(246, 198, 97, 25);
		contentPane.add(btnCourierNew);
		
		JButton btnInsLabelSchreiben = new JButton("Ins label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText(textField.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(22, 265, 144, 25);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");
			}
		});
		btnTextImLabel.setBounds(178, 265, 144, 25);
		contentPane.add(btnTextImLabel);
		
		JLabel lblAufgabeschriftfarbendern = new JLabel("Aufgabe 3:Schriftfarbe \u00E4ndern");
		lblAufgabeschriftfarbendern.setBounds(29, 315, 228, 16);
		contentPane.add(lblAufgabeschriftfarbendern);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.RED);
			}
		});
		btnRot_1.setBounds(22, 357, 97, 25);
		contentPane.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(136, 357, 97, 25);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(246, 357, 97, 25);
		contentPane.add(btnSchwarz);
		
		JButton button_14 = new JButton("+");
		button_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel.getFont().getSize();
				int font = lblNewLabel.getFont().getStyle();
				String family = lblNewLabel.getFont().getFamily();
				lblNewLabel.setFont(new Font(family,font,groesse + 1));
			}
		});
		button_14.setBounds(22, 438, 144, 25);
		contentPane.add(button_14);
		
		JButton button_15 = new JButton("-");
		button_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel.getFont().getSize();
				int font = lblNewLabel.getFont().getStyle();
				String family = lblNewLabel.getFont().getFamily();
				lblNewLabel.setFont(new Font(family,font,groesse - 1));
			}
		});
		button_15.setBounds(178, 438, 144, 25);
		contentPane.add(button_15);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);;
			}
		});
		btnLinksbndig.setBounds(22, 533, 97, 25);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);;
			}
		});
		btnZentriert.setBounds(136, 533, 97, 25);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);;
			}
		});
		btnRechtsbndig.setBounds(246, 533, 97, 25);
		contentPane.add(btnRechtsbndig);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(29, 583, 314, 45);
		contentPane.add(btnExit);
		
		JLabel lblAufgaebSchiftgre = new JLabel("Aufgaeb 4: Schiftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgaebSchiftgre.setBounds(29, 408, 228, 16);
		contentPane.add(lblAufgaebSchiftgre);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setBounds(29, 504, 199, 16);
		contentPane.add(lblAufgabeTextausrichtung);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setBounds(29, 565, 199, 16);
		contentPane.add(lblAufgabeProgramm);
		
		JLabel lblAufgabeTet = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeTet.setBounds(29, 179, 316, 16);
		contentPane.add(lblAufgabeTet);
		
		textField = new JTextField();
		textField.setBounds(29, 230, 273, 22);
		contentPane.add(textField);
		textField.setColumns(10);
	}
}
