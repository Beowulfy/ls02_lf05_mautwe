package Angestelltenverwaltung;

///Implementierung der Klasse “Angestellter”
public class Angestellter
{
//Attribute
private String name;
private double gehalt;

public Angestellter()
{ }
public Angestellter(String name)
{ this.name = name; }
public Angestellter(String name, double gehalt)
{
this.gehalt = gehalt;
this.name = name;
}

//Methoden
public void setName(String name) {
this.name = name;
}
public String getName() {
return this.name;
}
public void setGehalt(double gehalt) {
this.gehalt = gehalt;
}
public double getGehalt() {
return this.gehalt;
	}
}