package Rechteck;

public class Rechteck {

	private double LengthB;
	private double LengthA;
    
	public Rechteck(double LengthA, double LengthB)
	{
	  this.setLengthA(LengthA);
	  this.setLengthB(LengthB);
	}
	
	public double getLengthA() {
		return this.LengthA;
	}
	
	public double getLengthB() {
		return this.LengthB;
	}
	
	public void setLengthA(double LengthA) {
		if(LengthA > 0) {
			this.LengthA = LengthA;
		}
	}
	public void setLengthB(double LengthB) {
		if(LengthB > 0) {
			this.LengthB = LengthB;
		}		
	}
	
	public double getFlaeche() {
		return LengthA*LengthB;
	}
	
	public double getUmfang() {
		return 2*LengthA+2*LengthB;
	}
	
}
