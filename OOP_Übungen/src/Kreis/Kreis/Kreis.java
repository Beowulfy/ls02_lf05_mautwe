package Kreis.Kreis;

import java.awt.Point;

public class Kreis {

	private double radius;
	private Point mittelpunkt;
    
	public Kreis(double radius, Point mittelpunkt)
	{
	  setMittelpunkt(mittelpunkt);
	  setRadius(radius);
	}
	
	public void setRadius(double radius) {
		if(radius > 0)
			this.radius = radius;
		else
			this.radius = 0;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getDurchmesser() {
		return this.radius * 2;
	}
	
	public double getFlaeche() {
		return Math.PI * Math.pow(this.radius, 2);
	}
	
	public double getUmfang() {
		return 2 * Math.PI * this.radius;
	}

	public void setMittelpunkt(Point mittelpunkt) {
		this.mittelpunkt = mittelpunkt; 
	}
	
	public Point getMittelpunkt() {
		return mittelpunkt;
	}
}
