package Kreis.Kreis;

import java.awt.Point;

public class Testkreis {

	public static void main(String[] args) {
		
		Kreis k = new Kreis(50,new Point(3,4));
        System.out.println("Fl�che: " + k.getFlaeche());
        System.out.println("Umfang: " + k.getUmfang());
        System.out.println("Durchmesser: " + k.getDurchmesser());
		
	}

}
