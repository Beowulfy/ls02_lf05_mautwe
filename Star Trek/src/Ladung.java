/**
 * Klasse der Ladung
 * @author Maut,Wolf-Erik
 *
 */
public class Ladung {
	
	/**
	 * Parameter f�r die Klasse Ladung.
	 */
	private String bezeichnung;
	private int menge;
	
	public Ladung() {}
	
	
	/**
	 * Konstruktor f�r die Ladung.
	 * @param bezeichnung
	 * @param menge
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	/**
	 * Getter und Setter f�r die Ladung (Menge und Bezeichnung).
	 * @return
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	
	public String toString() {
		return bezeichnung + " = " +menge;
	}
}
