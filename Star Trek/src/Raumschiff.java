import java.util.ArrayList;
import java.lang.Math;
import java.util.Random;


/** 
 * Klasse Raumschiff
 * @author Wolf-Erik Maut
 */
public class Raumschiff {

	/**
	 * Parameter f�r die Klasse Raumschiff. 
	 */
	private String nameRaumschiff;
	private int photonentorpedos;
	private	int schutzschildeinProzent;
	private int huelleinProzent;
	private int energieversorgunginProzent;
	private int lebenserhaltunginProzent;
	private int reparaturAndroiden;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsVerzeichniss;
	
	

	public Raumschiff() {}
	
	
	/**
	 * Konstruktor f�r Raumschiffe.
	 * @param nameRaumschiff
	 * @param photonentorpedos
	 * @param schutzschildeinProzent
	 * @param huelleinProzent
	 * @param energieversorgunginProzent
	 * @param lebenserhaltunginProzent
	 * @param reparaturAndroiden
	 * @author Wolf-Erik Maut
	 */
	public Raumschiff(String nameRaumschiff, int photonentorpedos, int schutzschildeinProzent, int huelleinProzent, int energieversorgunginProzent, int lebenserhaltunginProzent, int reparaturAndroiden) {
		this.nameRaumschiff = nameRaumschiff;
		this.photonentorpedos = photonentorpedos;
		this.schutzschildeinProzent = schutzschildeinProzent;
		this.huelleinProzent = huelleinProzent;
		this.energieversorgunginProzent = energieversorgunginProzent;
		this.lebenserhaltunginProzent = lebenserhaltunginProzent;
		this.reparaturAndroiden = reparaturAndroiden;
		this.ladungsVerzeichniss = new ArrayList <Ladung>();
		this.broadcastKommunikator = new ArrayList <String>();
		
	}
	
	/**
	 * Methode die Zust�nde wiederzugeben. 
	 */
	public void getZustaende() {
		System.out.println("\n"+ this.nameRaumschiff + "\n");
		System.out.println("Schutzschilde: "+ schutzschildeinProzent);
		System.out.println("H�lle: "+ huelleinProzent);
		System.out.println("Energieversorgung: "+ energieversorgunginProzent);
		System.out.println("Lebenserhaltung: " + lebenserhaltunginProzent +"\n");
		
	}
	
	/**
	 * Getter und Setter f�r die Parameter. 
	 * @return
	 * 
	 */
	public String getNameRaumschiff() {
		return nameRaumschiff;
	}
	public void setNameRaumschiff(String nameRaumschiff) {
		this.nameRaumschiff = nameRaumschiff;
	}
	public int getPhotonentorpedos() {
		return photonentorpedos;
	}
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	public int getSchutzschildeinProzent() {
		return schutzschildeinProzent;
	}
	public void setSchutzschildeinProzent(int schutzschildeinProzent) {
		this.schutzschildeinProzent = schutzschildeinProzent;
	}
	public int getHuelleinProzent() {
		return huelleinProzent;
	}
	public void setHuelleinProzent(int huelleinProzent) {
		this.huelleinProzent = huelleinProzent;
	}
	public int getEnergieversorgunginProzent() {
		return energieversorgunginProzent;
	}
	public void setEnergieversorgunginProzent(int energieversorgunginProzent) {
		this.energieversorgunginProzent = energieversorgunginProzent;
	}
	public int getLebenserhaltunginProzent() {
		return lebenserhaltunginProzent;
	}
	public void setLebenserhaltunginProzent(int lebenserhaltunginProzent) {
		this.lebenserhaltunginProzent = lebenserhaltunginProzent;
	}
	public int getReparaturAndroiden() {
		return reparaturAndroiden;
	}
	public void setReparaturAndroiden(int reparaturAndroiden) {
		this.reparaturAndroiden = reparaturAndroiden;
	}
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	/**
	 * Methode zur wiedergabe des Ladungsverzeichnisses.
	 */
	public void printLadungsVerzeichniss() {
		for(int i = 0; i<ladungsVerzeichniss.size();i++) {
			System.out.println(ladungsVerzeichniss.get(i).toString());
		} 
		
	}
	
	/**
	 * 
	 * @param ladung
	 */
	public void addLadung(Ladung ladung) {
		this.ladungsVerzeichniss.add(ladung);
	
	}
		
	/**
	 * Methode um nicht vorhandene Ladung auszublenden. 
	 */
	public void removeLadung() {
		for(int i = 0; i<ladungsVerzeichniss.size();i++) {
			Ladung tmp = ladungsVerzeichniss.get(i);
				if (tmp.getMenge() <= 0) {
					ladungsVerzeichniss.remove(i);
					}
			}
		}
	
	/**
	 * Methode zum verteilen des Schadens bei einem Treffer.
	 * @param raumschiff
	 */
	private void treffer(Raumschiff raumschiff) {
		String s =raumschiff.getNameRaumschiff()+ " wurde getroffen!";
		broadcastKommunikator.add(s);
		raumschiff.setSchutzschildeinProzent(raumschiff.getSchutzschildeinProzent() -50);
		if (raumschiff.getSchutzschildeinProzent() <= 0) {
			raumschiff.setHuelleinProzent(raumschiff.getHuelleinProzent()-50);
			raumschiff.setEnergieversorgunginProzent(raumschiff.getEnergieversorgunginProzent()-50);
			if (raumschiff.getHuelleinProzent() <= 0) {
				raumschiff.setLebenserhaltunginProzent(0) ;
				System.out.println("Ihr seid tot!\n");
			}
					
			}
	
	}	
	
	/**
	 * Methode zum schie�en der Photonentorpedos.
	 * Gibt ein Print wieder sollten keine Torpedos mehr vorhanden sein.
	 * @param raumschiff
	 */
	public void torpedosSchie�en(Raumschiff raumschiff) {
		if (photonentorpedos > 0) {
			photonentorpedos -=1;
			System.out.println("\n" + "Photonentorpedo abgeschossen");
			treffer(raumschiff);
		}
		else System.out.println("\n" + "-=*Click*=-");	
	}
	
	/**
	 * Methode zum schie�en der Phaserb�nke.
	 * @param raumschiff
	 */
	public void phaserbaenkeSchie�en(Raumschiff raumschiff) {
		if (energieversorgunginProzent >= 50) {
			energieversorgunginProzent -= 50;
			System.out.println("\n" + "Phaserb�nke abgeschossen");
			treffer(raumschiff);
		}
		else System.out.println("\n" + "-=*Click*=-");
		
	}
	
	
	//*public void reparatur(boolean schutzschildeinProzent, boolean huelleinProzent, boolean energieversorgunginProzent, int anzahlAI) {
	//	Random zufallsGenerator = new Random();
	//	int zahl = zufallsGenerator.nextInt(101);
	//	int ergebniss = 0;
	//	int anzahlBoolean = 0;
	//	if(anzahlAI > this.reparaturAndroiden) {
	//		anzahlAI = this.reparaturAndroiden;
	//	}
	//	if(schutzschildeinProzent) {
	//		anzahlBoolean = anzahlBoolean + 1;
	//	}
	//	if(huelleinProzent) {
	//		anzahlBoolean = anzahlBoolean + 1;	
	//	}
	//	if(energieversorgunginProzent) {
	//		anzahlBoolean = anzahlBoolean + 1;	
	//	}
	//	ergebniss = (zahl * Math.abs(anzahlAI)) / Math.abs(anzahlBoolean);
	//	if(schutzschildeinProzent) {
	//		this.schutzschildeinProzent = this.schutzschildeinProzent + ergebniss;
	//	}
	//	if(huelleinProzent) {
	//		this.huelleinProzent = this.huelleinProzent + ergebniss;
	//	}
	//	if(energieversorgunginProzent) {
	//		this.energieversorgunginProzent = this.energieversorgunginProzent + ergebniss;
	//	}
		
		
		
	//}
}
