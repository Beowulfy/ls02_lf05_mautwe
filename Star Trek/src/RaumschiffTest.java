/**
 * Erstellte Raumschiffe + Ladungen und ihre Aktionen.
 * @author Wolf-Erik Maut
 */
public class RaumschiffTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		/**
		 * Konstruierte Raumschiffe mit ihren Werten.
		 */
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 1, 100, 100, 100, 100, 2);
		
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 50, 80, 100, 5);
		
		
		//Testladungen
		//Ladung l = new Ladung("Bitcoins", 0);
		//Ladung l2 = new Ladung("Dogecoins", 500);
		//Ladung l3 = new Ladung("Coinminer-Sklaven", 1500);
		//r.addLadung(l);
		//r.addLadung(l2);
		//r.addLadung(l3);
		
		
		/**
		 * Konstruierte Ladungen.
		 */
		Ladung l = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l1 = new Ladung("Bat'leth", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 0);
		Ladung l4 = new Ladung("Plasma-Waffe", 50);
		Ladung l5 = new Ladung("Forschungssonde", 35);
		Ladung l6 = new Ladung("Photonentorpedo", 3);
		
		/**
		 * Status&Ladung der Klingonen werden wiedergegeben
		 */
		// Klingonen //
		//Status&Ladung Klingonen
		klingonen.getZustaende();
		klingonen.printLadungsVerzeichniss();
		//Ladung Klingonen
		klingonen.addLadung(l);
		klingonen.addLadung(l1);
		klingonen.printLadungsVerzeichniss();	
		
		/**
		 * Status&Ladung der Romulaner werden wiedergegeben
		 */
		// Romulaner //
		//Status&Ladung Romulaner
		romulaner.getZustaende();
		romulaner.printLadungsVerzeichniss();
		//Ladung Romulaner
		romulaner.addLadung(l2);
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.printLadungsVerzeichniss();
		
		/**
		 * Status&Ladung der Vulkanier werden wiedergegeben.
		 */
		// Vulkanier //
		//Status&Ladung Vulkanier
		vulkanier.getZustaende();
		vulkanier.printLadungsVerzeichniss();
		//Ladung Vulkanier
		vulkanier.addLadung(l5);
		vulkanier.addLadung(l6);
		vulkanier.printLadungsVerzeichniss();

		//r.printLadungsVerzeichniss();
		//r2.torpedosSchießen(r);
		//r2.phaserbaenkeSchießen(r);
		
		//r.removeLadung(l);
		
		/**
		 * Abfolge an Aktionen.		
		 */
		//Klingonen schießen
				klingonen.torpedosSchießen(romulaner);
				
		//Romulaner schießen zurück
				romulaner.phaserbaenkeSchießen(klingonen);
				
		//Vulkanier jammern rum
				//vulkanier.
				
		//Klingonen Zustand + Ladung		
				klingonen.getZustaende();
				klingonen.printLadungsVerzeichniss();
		
		//Vulkanier reparieren sich
				//vulkanier.reparatur();
		
		//Vulkanier laden Torpedos
				vulkanier.removeLadung();
				vulkanier.printLadungsVerzeichniss();
		
		//Klingonen schießen 2 weitere Torpedos
				klingonen.torpedosSchießen(romulaner);
				klingonen.torpedosSchießen(romulaner);
				
		//Zustände aller Schiffe
				klingonen.getZustaende();
				romulaner.getZustaende();
				vulkanier.getZustaende();
				
	}

}
